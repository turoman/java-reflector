package com.turoman.reflector;

import java.util.List;

public class ApiInterface {
	
	/**
     * Constructor method. Takes the name of the interface as argument.
     * @param name 
     */
    public ApiInterface(String name, List<ApiMethod> methods){
        this.Name = name;
        this.methods = methods;
    }
    
    /**
     * Private field to store the name of this interface.
     */
    private String Name;
    
    /**
     * The list of methods for this interface.
     */
    public List<ApiMethod> methods;    
        
    /**
     * Returns the name of the interface. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };

}
