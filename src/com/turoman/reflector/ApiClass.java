package com.turoman.reflector;

import java.util.List;

public class ApiClass {
	
    /**
     * Constructor method. Takes the name of the class as argument.
     * @param name 
     */
    public ApiClass(String name, List<ApiMethod> methods){
        this.Name = name;
        this.methods = methods;
    }
    
    /**
     * Private field to store the name of this class.
     */
    private String Name;
    
    /**
     * The list of methods for this class.
     */
    public List<ApiMethod> methods;    
        
    /**
     * Returns the name of the class. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };

}
