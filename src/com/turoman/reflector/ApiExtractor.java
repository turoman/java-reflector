package com.turoman.reflector;

import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Extracts the API information from the jar file supplied as argument to an XML
 * file. Only information about public members is extracted.
 */
public class ApiExtractor {

	public static void main(String[] args) {

		try {
			if (args.length > 0) {
				Path path = new File(args[0]).toPath();

				if (Files.exists(path)) {
					readApiInfo(path.toString());
				} else {
					System.out.println("The path supplied as argument does not exist.");
				}
			} else {
				System.out.println("Please specify an input path as argument.");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void readApiInfo(String pathToJar) {
		try {
			// Add the API library object
			ApiLibrary apilib = new ApiLibrary(
					"Java Object Reference",
					new ArrayList<>(),
					new ArrayList<>(),
					new ArrayList<>());						

			// Do the extraction
			apilib.classes = extractClasses(pathToJar, apilib);
			apilib.interfaces = extractInterfaces(pathToJar, apilib);
			apilib.enumerations = extractEnumerations(pathToJar, apilib);

			// Create the XML document
			createXML(pathToJar, apilib);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void createXML(String pathToJar, ApiLibrary apilib) {
		try {
			// Initialize objects required to create the XML file
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// Create the XML root element
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("apidoc");
			doc.appendChild(rootElement);

			// Create the "libraries" element
			Element librariesElem = doc.createElement("libraries");
			Element libraryElem = addXMLLibrary(pathToJar, apilib, doc);
			librariesElem.appendChild(libraryElem);
			rootElement.appendChild(librariesElem);

			// Save the XML file to disk
			saveXML(pathToJar, doc);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static Element addXMLLibrary(String pathToJar, ApiLibrary apilib, Document doc) throws Exception {
		try {
			// Create the "library" element
			Element libraryElem = doc.createElement("library");
			libraryElem.setAttribute("name", "Java Object Reference");
			Element libdescElem = doc.createElement("description");
			libdescElem.appendChild(doc.createTextNode(
					"This API structure was generated from " + pathToJar + " on " + LocalDateTime.now()));
			libraryElem.appendChild(libdescElem);

			// Add the classes
			if (!apilib.classes.isEmpty()) {
				Element classesElem = doc.createElement("classes");
				for (ApiClass apiClass : apilib.classes) {
					Element classElem = addXMLClass(apiClass, doc);
					classesElem.appendChild(classElem);
				}
				libraryElem.appendChild(classesElem);
			}
			
			// Add the interfaces
			if (!apilib.interfaces.isEmpty()) {
				Element interfacesElem = doc.createElement("interfaces");
				for (ApiInterface apiInterface : apilib.interfaces) {
					Element interfaceElem = addXMLInterface(apiInterface, doc);
					interfacesElem.appendChild(interfaceElem);
				}
				libraryElem.appendChild(interfacesElem);
			}
			
			// Add the enumerations
			if (!apilib.enumerations.isEmpty()) {
				Element enumerationsElem = doc.createElement("enumerations");
				for (ApiEnumeration apiEnumeration : apilib.enumerations) {
					Element enumElem = addXMLEnumeration(apiEnumeration, doc);
					enumerationsElem.appendChild(enumElem);
				}
				libraryElem.appendChild(enumerationsElem);
			}

			return libraryElem;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static Element addXMLClass(ApiClass apiClass, Document doc) throws Exception {
		try {
			Element classElem = doc.createElement("class");
			classElem.setAttribute("name", apiClass.getName());
			if (!apiClass.methods.isEmpty()) {
				// Add the class methods
				Element apiMethods = doc.createElement("methods");
				for (ApiMethod apiMethod : apiClass.methods) {
					Element methodElem = addXMLMethod(apiMethod, doc);					
					apiMethods.appendChild(methodElem);
				}
				classElem.appendChild(apiMethods);
			}
			return classElem;			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	private static Element addXMLInterface(ApiInterface apiInterface, Document doc) throws Exception {
		try {
			Element interfaceElem = doc.createElement("interface");
			interfaceElem.setAttribute("name", apiInterface.getName());
			if (!apiInterface.methods.isEmpty()) {
				// Add the methods
				Element apiMethods = doc.createElement("methods");
				for (ApiMethod apiMethod : apiInterface.methods) {
					Element methodElem = addXMLMethod(apiMethod, doc);					
					apiMethods.appendChild(methodElem);
				}
				interfaceElem.appendChild(apiMethods);
			}
			return interfaceElem;			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static Element addXMLEnumeration(ApiEnumeration apiEnumeration, Document doc) throws Exception {
		try {
			Element enumElem = doc.createElement("enumeration");
			enumElem.setAttribute("name", apiEnumeration.getName());
			if (!apiEnumeration.members.isEmpty()) {
				// Add the members
				Element enumMembers = doc.createElement("members");
				for (ApiEnumMember apiEnumMember : apiEnumeration.members) {
					Element memberElem = doc.createElement("member");
					memberElem.setAttribute("name", apiEnumMember.getName());
					enumMembers.appendChild(memberElem);
				}
				enumElem.appendChild(enumMembers);
			}
			return enumElem;			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	private static Element addXMLMethod(ApiMethod apiMethod, Document doc) throws Exception {
		try {
			Element methodElem = doc.createElement("method");
			methodElem.setAttribute("name", apiMethod.getName());
			methodElem.setAttribute("returnType", apiMethod.getReturnType());
			if (!apiMethod.parameters.isEmpty()) {
				// Add the method parameters
				Element metParamsElem = doc.createElement("parameters");
				for (ApiParameter apiParameter : apiMethod.parameters) {
					Element metParamElem = doc.createElement("parameter");
					metParamElem.setAttribute("name", apiParameter.getName());
					metParamElem.setAttribute("dataType", apiParameter.getDataType());
					metParamsElem.appendChild(metParamElem);
				}
				methodElem.appendChild(metParamsElem);
			}
			return methodElem;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static void saveXML(String pathToJar, Document doc) {
		try {
			// Write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			String filename = Paths.get(pathToJar).getFileName().toString();
			filename = filename.substring(0, filename.length() - 3);
			System.out.println("Writing data to " + filename + "xml");
			StreamResult result = new StreamResult(new File(filename + "xml"));
			transformer.transform(source, result);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static List<ApiClass> extractClasses(String pathToJar, ApiLibrary apilib) throws Exception {
		try {
			JarFile jarFile = new JarFile(pathToJar);
			Enumeration<JarEntry> e = jarFile.entries();
			URL[] urls = { new URL("jar:file:" + pathToJar + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			System.out.println("=====Classes=====");
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<?> c = cl.loadClass(className);

				int mods = c.getModifiers();
				if (Modifier.isPublic(mods) && !c.isEnum() && !c.isInterface()) {
					ApiClass cls = new ApiClass(c.getSimpleName(), new ArrayList<>());					
					System.out.println("Class " + c.getSimpleName());
					cls.methods = extractMethods(c, cls);
					apilib.classes.add(cls);
				}
			}

			jarFile.close();

			// Sort the classes alphabetically by name
			apilib.classes.sort(Comparator.comparing(ApiClass::getName));
			return apilib.classes;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static List<ApiInterface> extractInterfaces(String pathToJar, ApiLibrary apilib) throws Exception {
		try {
			JarFile jarFile = new JarFile(pathToJar);
			Enumeration<JarEntry> e = jarFile.entries();
			URL[] urls = { new URL("jar:file:" + pathToJar + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			System.out.println("=====Interfaces=====");		
			
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<?> c = cl.loadClass(className);

				int mods = c.getModifiers();
				if (Modifier.isPublic(mods) && !c.isEnum() && c.isInterface()) {
					ApiInterface intf = new ApiInterface(c.getSimpleName(),new ArrayList<>());					
					System.out.println("Interface " + c.getSimpleName());
					intf.methods = extractMethods(c, intf);	
					apilib.interfaces.add(intf);			
				}
			}
					
			jarFile.close();
			
			// Sort the interfaces alphabetically by name
			apilib.interfaces.sort(Comparator.comparing(ApiInterface::getName));
			return apilib.interfaces;
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static List<ApiEnumeration> extractEnumerations(String pathToJar, ApiLibrary apilib) throws Exception {
		try {
			JarFile jarFile = new JarFile(pathToJar);
			Enumeration<JarEntry> e = jarFile.entries();
			URL[] urls = { new URL("jar:file:" + pathToJar + "!/") };
			URLClassLoader cl = URLClassLoader.newInstance(urls);

			System.out.println("=====Enumerations=====");
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if (je.isDirectory() || !je.getName().endsWith(".class")) {
					continue;
				}
				// -6 because of .class
				String className = je.getName().substring(0, je.getName().length() - 6);
				className = className.replace('/', '.');
				Class<?> c = cl.loadClass(className);

				int mods = c.getModifiers();
				if (Modifier.isPublic(mods) && c.isEnum()) {
					ApiEnumeration apiEnum = new ApiEnumeration(c.getSimpleName(), new ArrayList<>());	
					System.out.println(c.getSimpleName());
					Object[] enumvalues = c.getEnumConstants();
					for (int i = 0; i < enumvalues.length; i++) {
						ApiEnumMember enumMember = new ApiEnumMember(enumvalues[i].toString());
						System.out.println("   " + enumvalues[i].toString());
						apiEnum.members.add(enumMember);
					}
					apilib.enumerations.add(apiEnum);
					apiEnum.members.sort(Comparator.comparing(ApiEnumMember::getName));
				}
			}		
			jarFile.close();
			apilib.enumerations.sort(Comparator.comparing(ApiEnumeration::getName));
			return apilib.enumerations;			
			
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static List<ApiMethod> extractMethods(Class<?> c, ApiClass cls) throws Exception {
		try {
			System.out.println("--Methods--");
			if (c.getDeclaredMethods().length > 0) {
				Method[] methods = c.getDeclaredMethods();

				for (int i = 0; i < methods.length; i++) {
					Method m = methods[i];
					int mod = m.getModifiers();
					if (Modifier.isPublic(mod)) {
						ApiMethod apiMethod = new ApiMethod(m.getName(), m.getGenericReturnType().getTypeName(),
								new ArrayList<>());
						System.out.println("    public " + m.getGenericReturnType().getTypeName() + " " + m.getName());
						apiMethod.parameters = extractParameters(m, apiMethod);
						cls.methods.add(apiMethod);
					}
				}
			}

			// Sort the methods alphabetically by name
			cls.methods.sort(Comparator.comparing(ApiMethod::getName));
			return cls.methods;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}
	
	private static List<ApiMethod> extractMethods(Class<?> c, ApiInterface intf) throws Exception {
		try {
			System.out.println("--Methods--");
			if (c.getDeclaredMethods().length > 0) {
				Method[] methods = c.getDeclaredMethods();

				for (int i = 0; i < methods.length; i++) {
					Method m = methods[i];
					int mod = m.getModifiers();
					if (Modifier.isPublic(mod)) {
						ApiMethod apiMethod = new ApiMethod(m.getName(), m.getGenericReturnType().getTypeName(),
								new ArrayList<>());
						System.out.println("    public " + m.getGenericReturnType().getTypeName() + " " + m.getName());
						apiMethod.parameters = extractParameters(m, apiMethod);
						intf.methods.add(apiMethod);
					}
				}
			}

			// Sort the methods alphabetically by name
			intf.methods.sort(Comparator.comparing(ApiMethod::getName));
			return intf.methods;

		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	private static List<ApiParameter> extractParameters(Method m, ApiMethod apiMethod) throws Exception {
		try {
			Parameter[] parameters = m.getParameters();
			if (parameters.length > 0) {
				for (int j = 0; j < parameters.length; j++) {
					Parameter p = parameters[j];
					ApiParameter apiParam = new ApiParameter(p.getName(), p.getType().getSimpleName());
					System.out.println("         " + p.getName() + " " + p.getType().getSimpleName());
					apiMethod.parameters.add(apiParam);
				}
			}
			return apiMethod.parameters;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

}
