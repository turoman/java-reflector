package com.turoman.reflector;

import java.util.List;

public class ApiEnumeration {

	/**
     * Constructor method. Takes the name of the enumeration as argument.
     * @param name 
     */
    public ApiEnumeration(String name, List<ApiEnumMember> members){
        this.Name = name;
        this.members = members;
    }
    
    /**
     * Private field to store the name of this enumeration.
     */
    private String Name;
    
    /**
     * The list of members for this enumeration.
     */
    public List<ApiEnumMember> members;    
        
    /**
     * Returns the name of the enumeration. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };
}
