package com.turoman.reflector;

import java.util.List;

public class ApiLibrary {

    /**
     * Constructor method. Takes the name of the library as argument.
     * @param name 
     */
    public ApiLibrary(
    		String name, 
    		List<ApiClass> classes,
    		List<ApiInterface> interfaces,
    		List<ApiEnumeration> enumerations){
        this.Name = name;
        this.classes = classes;
        this.interfaces = interfaces;
        this.enumerations = enumerations;
    }
    
    /**
     * Private field to store the name of this library.
     */
    private String Name;
    
    /**
     * The list of classes for this library.
     */
    public List<ApiClass> classes;
    
    /**
     * The list of interfaces for this library.
     */
    public List<ApiInterface> interfaces;
    
    /**
     * The list of enumerations for this library.
     */
    public List<ApiEnumeration> enumerations;
        
    /**
     * Returns the name of the library. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };
}
