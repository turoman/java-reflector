package com.turoman.reflector;

public class ApiEnumMember {

	/**
     * Constructor method. Takes the name of the enumeration member as argument.
     * @param name 
     */
    public ApiEnumMember(String name){
        this.Name = name;
    }
    
    /**
     * Private field to store the name of this enumeration member.
     */
    private String Name;        
        
    /**
     * Returns the name of the enumeration member. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };
}
