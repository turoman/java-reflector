package com.turoman.reflector;

public class ApiParameter {
	
	/**
     * Constructor method. Takes the name of the parameter as argument.
     * @param name 
     */
    public ApiParameter(String name, String type){
        this.Name = name;
        this.DataType = type;
    }
    
    /**
     * Private field to store the name of this method.
     */
    private String Name;

    /**
     * Private field to store the type of this method.
     */
    private String DataType;
       
    /**
     * Returns the name of the parameter. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };
    
    /**
     * Returns the type of the parameter. 
     * @return
     */
    public String getDataType(){
        return this.DataType;
    };

}
