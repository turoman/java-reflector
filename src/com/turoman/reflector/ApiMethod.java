package com.turoman.reflector;

import java.util.List;

public class ApiMethod {

    /**
     * Constructor method. Takes the name of the method as argument.
     * @param name 
     */
    public ApiMethod(String name, String type, List<ApiParameter> parameters){
        this.Name = name;
        this.ReturnType = type;
        this.parameters = parameters;
    }
    
    /**
     * Private field to store the name of this method.
     */
    private String Name;
    
    /**
     * Private field to store the return type of this method.
     */
    private String ReturnType;
    
    /**
     * The list of parameters for this method.
     */
    public List<ApiParameter> parameters;    
        
    /**
     * Returns the name of the method. Used for sorting.
     * @return
     */
    public String getName(){
        return this.Name;
    };
    
    /**
     * Returns the return type of the method. Used for sorting.
     * @return
     */
    public String getReturnType(){
        return this.ReturnType;
    };
}
