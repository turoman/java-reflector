# Java Reflector

This Java application extracts public API information from .jar files to an XML file. More specifically, all public members are extracted that are considered to be any of the following:

- Classes
- Interfaces
- Enumerations and enumeration members
- For each class or interface, all public methods and return type of each method
- For each method, all parameters and their return type

## Prerequisites

- Java JDK must be installed on the local machine.
- To compile and run this application from Eclipse, Eclipse must be installed.

## How to compile and run with Eclipse

### Step 1: Import the project into Eclipse

1. Open Eclipse.
2. On the **File** menu, click **Open Projects from File System**.
3. Click **Directory** and browse for the directory where the source code is.
4. Click **Finish**.

### Step 2: Create a default configuration

1. On the **Run** menu, click **Run As > Run Configurations**.
2. Right-click **Java Application**, and select **New Configuration**.
3. On the **Main** tab, browse for the project, and then click **Search** and select the `com.turoman.reflector.ApiReflector` class as shown below.
4. On the **Arguments** tab, paste the path to the .jar file from which you want to extract the API information (for example, "c:\Java\jdk-12.0.2\lib\jrt-fs.jar"). Make sure that the path is enclosed within quotes.
5. Click **Apply**, and then click **Run**.

The application runs and creates an XML file in the working directory. Note that the XML file is generated without a schema and is not indented. To format the XML file, you could open it in an editor like Visual Studio code, right-click the editor, and select **Format** from the context menu.
